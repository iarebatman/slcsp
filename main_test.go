package main

import (
	"bufio"
	"bytes"
	"testing"
)

func TestFindSecondLowestPlanRate(t *testing.T) {
	ra := RateArea{
		State:           "ZZ",
		StateRateAreaID: "1",
	}
	planRateAreasMap := map[RateArea][]PlanRate{
		ra: []PlanRate{
			PlanRate{
				Area: ra,
				Rate: 197.3,
			},
			PlanRate{
				Area: ra,
				Rate: 197.3,
			},
			PlanRate{
				Area: ra,
				Rate: 201.1,
			},
		},
	}
	expected := 201.1
	result := findSecondLowestPlanRateByRateArea(ra, &planRateAreasMap)
	if result.Rate != expected {
		t.Fail()
	}
}

//What this is really testing is if we can properly
//deduce the correct RateArea if a ZipCode reference multiple
//Counties.  This is handled within the call to buildZipToRateAreaMap
//as it associates a unique set of RateAreas to a given Zip Code.
//If, at the end of this call, the list contains more than a single
//RateArea, it means that the answer is Ambiguous.  If it contains only
//One, it means that all of the RateAreas for the given Zip Code match
//And we can use the first one from our set.
func TestZipCodeWithinMoreThanOneCountyWithSameRateAreas(t *testing.T) {
	zipsCSV := [][]string{
		[]string{"12345", "ZZ", "0000", "Test", "1"},
		[]string{"12345", "ZZ", "0000", "Test", "1"},
		[]string{"12345", "ZZ", "0000", "Test", "1"},
	}
	expected := RateArea{State: "ZZ", StateRateAreaID: "1"}
	expectedCount := 1
	result := buildZipToRateAreaMap(zipsCSV)
	if len(result["12345"]) != expectedCount {
		t.Fail()
	}
	if result["12345"][0] != expected {
		t.Fail()
	}

	if zipContainsMultipleRateAreas("12345", &result) {
		t.Fail()
	}

}

//This is very similar to the test above.  We are expecting
//here that the Zip Code will be associated with two different RateAreas
//And that the correct RateArea is ambiguous
func TestZipCodeWithinMoreThanOneCountyWithDifferentRateAreas(t *testing.T) {
	zipsCSV := [][]string{
		[]string{"12345", "ZZ", "0000", "Test", "1"},
		[]string{"12345", "ZZ", "0000", "Test", "2"},
		[]string{"12345", "ZZ", "0000", "Test", "1"},
	}
	expectedCount := 2
	result := buildZipToRateAreaMap(zipsCSV)
	if len(result["12345"]) != expectedCount {
		t.Fail()
	}
	if !zipContainsMultipleRateAreas("12345", &result) {
		t.Fail()
	}
}

func TestBuildPlanToRateAreaMap(t *testing.T) {

	plansCSV := [][]string{
		[]string{"1", "ZZ", "Silver", "197.3", "10"},
		[]string{"1", "ZZ", "Bronze", "197.3", "10"},
		[]string{"1", "ZZ", "Silver", "201.1", "10"},
		[]string{"1", "ZZ", "Bronze", "197.3", "16"},
		[]string{"1", "ZZ", "Silver", "201.1", "2"},
		[]string{"1", "ZZ", "Silver", "299.3", "8"},
	}
	result := buildPlanToRateAreaMap(plansCSV)
	if plans, ok := result[RateArea{State: "ZZ", StateRateAreaID: "10"}]; ok {
		//Based on the data above, there should be 2
		//Plans for ("ZZ", "10")
		//Bronze should be filtered out entirely
		if len(plans) != 2 {
			t.Fail()
		}
	}
	//We should expect 3 different RateAreas to be present
	if len(result) != 3 {
		t.Fail()
	}
}

func TestOutputCSVShouldLeaveAmbiguousRateAreasEmpty(t *testing.T) {
	slcspCSV := [][]string{
		[]string{"12345", ""},
	}
	zipsCSV := [][]string{
		[]string{"12345", "ZZ", "0000", "Test", "1"},
		[]string{"12345", "ZZ", "0000", "Test", "2"},
		[]string{"12345", "ZZ", "0000", "Test", "1"},
	}

	plansCSV := [][]string{
		[]string{"1", "ZZ", "Silver", "197.3", "10"},
		[]string{"1", "ZZ", "Bronze", "197.3", "10"},
		[]string{"1", "ZZ", "Silver", "201.1", "10"},
		[]string{"1", "ZZ", "Bronze", "197.3", "16"},
		[]string{"1", "ZZ", "Silver", "201.1", "2"},
		[]string{"1", "ZZ", "Silver", "299.3", "8"},
	}
	zipRateAreasMap := buildZipToRateAreaMap(zipsCSV)
	planRateAreasMap := buildPlanToRateAreaMap(plansCSV)
	var out bytes.Buffer
	w := bufio.NewWriter(&out)
	r := bufio.NewReader(&out)
	outputResultsCSV(w, &slcspCSV, &zipRateAreasMap, &planRateAreasMap)
	outputRecords := readRecordsOrDie(r)

	if !(outputRecords[0][0] == "12345" && outputRecords[0][1] == "") {
		t.Fail()
	}
}
