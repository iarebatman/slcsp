package main

import (
	"encoding/csv"
	"io"
	"log"
	"os"
	"sort"
	"strconv"
)

//RateArea is a distinct area in which a rate is effective
type RateArea struct {
	State           string
	StateRateAreaID string
}

//PlanRate associates a Plan and Rate with a particular RateArea
type PlanRate struct {
	PlanID     string
	Area       RateArea
	MetalLevel string
	Rate       float64
}

//ByRate is used as an idiomatic way to provide sorting functionality within Go.
//It's defined as a collection of PlanRate objects.
type ByRate []PlanRate

//By providing these methods for the ByRate type, we implement the sort.Interface
//Which allows us to easily sort all of the plans in a collection by their Rate
//NOTE: Filtering out non-Silver plans as well as grouping by RateAreas are done
//		outside of these methods.
func (r ByRate) Len() int           { return len(r) }
func (r ByRate) Swap(i, j int)      { r[i], r[j] = r[j], r[i] }
func (r ByRate) Less(i, j int) bool { return r[i].Rate < r[j].Rate }

//readRecordsOrDie is a non-public method that expects to be able to read
//all of the lines from the provider io.Reader.
//I was tempted to offer a memory-constrained approach to this problem,
//but seeing as how the files were quite small relative to memory capacity
//that is common by now, I opted for the 'faster' approach.
//It ignores the first row, as they are all headers
func readRecordsOrDie(in io.Reader) [][]string {
	r := csv.NewReader(in)
	records, err := r.ReadAll()
	if err != nil {
		log.Fatal("Couldn't read ")
	}
	return records[1:]
}

//readFileRecordsOrDie is a wrapper for readRecordsOrDie.
//They are split up to aid in testing purposes.
func readFileRecordsOrDie(fileName string) [][]string {
	if f, err := os.Open(fileName); err == nil {
		return readRecordsOrDie(f)
	}
	return nil
}

//buildZipToRateAreaMap accepts all of the records from the zips.csv file
//and builds a map that allows us to quickly look up the unique set of
//RateAreas that exist for any given zip code.
func buildZipToRateAreaMap(records [][]string) map[string][]RateArea {
	res := map[string][]RateArea{}
	for _, countyRateRecord := range records {
		zip := countyRateRecord[0]
		state := countyRateRecord[1]
		rateArea := countyRateRecord[4]
		var rateAreas []RateArea
		if ra, ok := res[zip]; ok {
			rateAreas = ra
		}
		found := false
		for _, ra := range rateAreas {
			if ra.State == state && ra.StateRateAreaID == rateArea {
				found = true
				break
			}
		}
		if !found {
			rateAreas = append(rateAreas, RateArea{
				State:           state,
				StateRateAreaID: rateArea,
			})
		}
		res[zip] = rateAreas
	}
	return res
}

//buildPlanToRateAreaMap accepts all of the records from the plans.csv file
//and builds a map that allows us to quickly look up all available plans
//that exist for a given RateArea.
func buildPlanToRateAreaMap(records [][]string) map[RateArea][]PlanRate {
	res := map[RateArea][]PlanRate{}
	for _, planRecord := range records {
		planID := planRecord[0]
		state := planRecord[1]
		metalLevel := planRecord[2]
		if metalLevel != "Silver" {
			continue
		}

		if rate, err := strconv.ParseFloat(planRecord[3], 32); err == nil {
			rateArea := planRecord[4]
			ra := RateArea{
				State:           state,
				StateRateAreaID: rateArea,
			}
			res[ra] = append(res[ra], PlanRate{
				Area:       ra,
				PlanID:     planID,
				MetalLevel: metalLevel,
				Rate:       rate,
			})
		}
	}
	return res
}

//zipContainsMultipleRateAreas is just a conditional added to ease testing.
//It has an assumption built in that we will never receive a zip code that is not
//contained within the zipRateAreasMap.
func zipContainsMultipleRateAreas(zip string, zipRateAreasMap *map[string][]RateArea) bool {
	if rateAreas, ok := (*zipRateAreasMap)[zip]; ok {
		if len(rateAreas) > 1 {
			return true
		}
	}
	return false
}

//findSecondLowestPlanRateByRateArea returns the second lowest Rate within planRateAreasMap for a given RateArea
//Instead of building a 'set' for every list we process here, I decided to
//short-cut the logic since I have to do a Sort anyway.
//It simply returns the first plan that is higher in price than the least expensive plan in the given RateArea
//It has an assumption built in that there will always be at least 2 plans for any given RateArea.
//It WILL panic if this is not the case.
func findSecondLowestPlanRateByRateArea(rateArea RateArea, planRateAreasMap *map[RateArea][]PlanRate) *PlanRate {
	if plans, ok := (*planRateAreasMap)[rateArea]; ok {
		sort.Sort(ByRate(plans))
		firstRate := plans[0]
		for _, plan := range plans[1:] {
			if plan.Rate > firstRate.Rate {
				return &plan
			}
		}
	}
	return nil
}

//outputResultsCSV is a helper method to ease with testing.
//It accepts any io.Writer and writes the completed slcsp.csv file to it
//Using the provided parameters as sources.
func outputResultsCSV(out io.Writer, queries *[][]string, zipRateAreasMap *map[string][]RateArea, planRateAreasMap *map[RateArea][]PlanRate) {
	w := csv.NewWriter(out)
	defer w.Flush()

	w.Write([]string{"zipcode", "rate"})
	for _, queryRecord := range *queries {
		zip := queryRecord[0]
		if zipContainsMultipleRateAreas(zip, zipRateAreasMap) {
			w.Write([]string{zip, ""})
		} else {
			rateAreas := (*zipRateAreasMap)[zip]
			rateArea := rateAreas[0]
			planRate := findSecondLowestPlanRateByRateArea(rateArea, planRateAreasMap)
			if planRate == nil {
				//We don't have a single Plan for this RateArea
				w.Write([]string{zip, ""})
			} else {
				w.Write([]string{zip, strconv.FormatFloat(planRate.Rate, 'f', 2, 32)})
			}
		}
	}
}

func main() {
	queries := readFileRecordsOrDie("slcsp.csv")
	zipRecords := readFileRecordsOrDie("zips.csv")
	planRecords := readFileRecordsOrDie("plans.csv")

	zipRateAreasMap := buildZipToRateAreaMap(zipRecords)
	planRateAreasMap := buildPlanToRateAreaMap(planRecords)

	outputResultsCSV(os.Stdout, &queries, &zipRateAreasMap, &planRateAreasMap)
}
